//
//  VPNTableCell.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit

class VPNTableCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var btnDeleteKey: UIButton!
    weak var vpnTable: VPNTable?
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func reuseIdentifier()->String
    {
        return "savedkeysReuseIdentifier";
    }

    @IBAction func btnDeleteClicked(_ sender: UIButton)
    {
        if let the_table = vpnTable{
            the_table.deleteKey(atIndex: btnDeleteKey.tag)
        }
    }
}
