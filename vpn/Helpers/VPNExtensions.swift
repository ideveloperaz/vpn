//
//  VPNExtensions.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit
import KeychainSwift

extension UIViewController
{
    
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    // -------------------
    func dismissControllerWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissController))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissController(gestureRecognizer: UITapGestureRecognizer)
    {
        
        let view = gestureRecognizer.view
        let loc = gestureRecognizer.location(in: view)
        let subview = view?.hitTest(loc, with: nil) // note: it is a `UIView?`
        
        if subview! == view {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension KeychainSwift
{

    //[Rufat A] KeychainSwift my favorit pod but lack of getting PersistentRef, lets fix that :-)
    /*added PersistentRef*/
    open func getPersistentRef(_ key: String) -> Data?
    {
        let prefixedKey = key
        
        var query: [String: Any] = [
            KeychainSwiftConstants.klass       : kSecClassGenericPassword,
            KeychainSwiftConstants.attrAccount : prefixedKey,
            kSecReturnPersistentRef as String  : kCFBooleanTrue,
            KeychainSwiftConstants.matchLimit  : kSecMatchLimitOne
        ]
        
        query = addAccessGroupWhenPresent(query)
        query = addSynchronizableIfRequired(query, addingItems: false)
        //lastQueryParameters = query
        
        var result: AnyObject?
        
        lastResultCode = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if lastResultCode == noErr { return result as? Data }
        
        return nil
    }
    
    func addAccessGroupWhenPresent(_ items: [String: Any]) -> [String: Any]
    {
        guard let accessGroup = accessGroup else { return items }
        
        var result: [String: Any] = items
        result[KeychainSwiftConstants.accessGroup] = accessGroup
        return result
    }
    
    func addSynchronizableIfRequired(_ items: [String: Any], addingItems: Bool) -> [String: Any]
    {
        if !synchronizable { return items }
        var result: [String: Any] = items
        result[KeychainSwiftConstants.attrSynchronizable] = addingItems == true ? true : kSecAttrSynchronizableAny
        return result
    }
    
}
