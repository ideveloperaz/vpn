//
//  VPNIntrinsicTableView.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit

//this tableview subclass used to show its full height inside external scrollview
class VPNIntrinsicTableView: UITableView
{

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override var contentSize:CGSize
        {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize
        {
        get{
            self.layoutIfNeeded()
            return CGSize.init(width: UIViewNoIntrinsicMetric, height: contentSize.height)
        }
    
    }

}
