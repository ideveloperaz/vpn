//
//  VPNFileManager.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit

class VPNFileManager: NSObject {
    
    // helps to construct path
    class func makePath(fileName: String, appendP12: Bool=true)->String?
    {
        let docDirectory = try? FileManager.default.url(for: .documentDirectory,
                                                        in: .userDomainMask,
                                                        appropriateFor: nil,
                                                        create: true)
        if let fileURL = (appendP12) ? docDirectory?.appendingPathComponent(fileName).appendingPathExtension("p12"):docDirectory?.appendingPathComponent(fileName) {
            return fileURL.path
        }
        return nil
    }

    // get document folder
    class func getDocs()->String?
    {
        let docDirectory = try? FileManager.default.url(for: .documentDirectory,
                                                        in: .userDomainMask,
                                                        appropriateFor: nil,
                                                        create: true)
        return docDirectory?.path
    }

    // write p12 key to file
    class func writeToFile(dataToWrite: Data?, fname: String)
    {
        
        let fileManager = FileManager()
        if let path = makePath(fileName: fname) {
            let fileAttributes = [ FileAttributeKey.protectionKey.rawValue : FileProtectionType.complete.rawValue]
            
            let wrote = fileManager.createFile(atPath: path, contents: dataToWrite,
                                                     attributes: fileAttributes)
            if wrote{
                print("Successfully and securely stored the file")
            }else{
                print("Failed to write the file")
            }
        }
    }
    
    // get stored p12 files from Docs
    class func getKeys()->[String]?
    {
        
        guard let the_docs = getDocs() else { return nil }
        
        let fileManager = FileManager()
        
        var key_files = [String]()

        do{
            let files = try fileManager.contentsOfDirectory(atPath: the_docs)
            
            for the_file in files {

                print("\(the_file)")
                
                if the_file.hasSuffix(".p12"){
                    print("\(the_file) found")
                    key_files.append(the_file)
                }
            }
            
            return key_files
        }catch let error as NSError  {
            print("Failed to remove the file \(error.localizedDescription)")
            return nil
        }
    }
    
    // delete file from local storage
    class func deleteFile(fname: String)
    {
        
        let fileManager = FileManager()
        if let path = makePath(fileName: fname, appendP12: false) {

            do{
                try fileManager.removeItem(atPath: path)
                
                print("Successfully removed the file")

            }catch let error as NSError {
                print("Failed to remove the file \(error.localizedDescription)")
            }
        }
    }
    
    // read file from loca lstorage
    class func readFile(fname: String)->Data?
    {
        
        let fileManager = FileManager()
        if let path = makePath(fileName: fname, appendP12: false) {
            
            print("Successfully read the file")

            return fileManager.contents(atPath: path)
        }
        
        return nil
    }
}
