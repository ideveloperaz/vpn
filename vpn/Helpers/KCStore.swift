//
//  KCStore.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit
import KeychainSwift

class KCStore: NSObject {

    class func setStringToKeychain(object: String, forKey: VPNStructs.KeychainKeys)
    {
        let keychain = KeychainSwift()
        
        keychain.set(object, forKey: forKey.rawValue)
    }
    
    class func getDataFromKeychain(forKey: VPNStructs.KeychainKeys) -> Data?
    {
        let keychain = KeychainSwift()
        
        if let the_data = keychain.getData(forKey.rawValue){
            return the_data
        }
        
        return nil
    }

    class func getRefFromKeychain(forKey: VPNStructs.KeychainKeys) -> Data?
    {
        let keychain = KeychainSwift()
        
        if let the_data = keychain.getPersistentRef(forKey.rawValue){
            return the_data
        }
        
        return nil
    }

    
    class func getStringFromKeychain(forKey: VPNStructs.KeychainKeys) -> String?
    {
        let keychain = KeychainSwift()
        
        if let the_string = keychain.get(forKey.rawValue){
            return the_string
        }
        
        return nil
    }
    
    class func clear()
    {
        let keychain = KeychainSwift()
        
        keychain.clear()
        //keychain.delete(MPStructs.KeychainKeys.shopping_cart_key.rawValue)
    }

}
