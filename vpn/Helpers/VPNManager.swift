//
//  VPNManager.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit
import NetworkExtension

class VPNManager: NSObject {

    var username, serverAddress, remoteIdentifier, localizedDescription, password, sharedSecret, certUrl: String?
    var passwordReference, sharedSecretReference: Data?
    var authenticationMethod: NEVPNIKEAuthenticationMethod?

    var vpnManager: NEVPNManager?
    var delegate: VPNDelegateProtocol?
    
    var keyInData: Data?

    let debug = 0
    
    override init()
    {
        super.init()
        let _ = self.addVPNObserver()
    }
    
    deinit
    {
        self.removeVPNObserver()
        vpnManager = nil
        print("deinit VPNManager")
    }
    
    // some vpn initialization and getting permission on vpn stuff
    func load(withKey: Data?)
    {
        
        if let the_key = withKey {
            self.keyInData = the_key
        }else{
            self.keyInData = nil
        }
        
        vpnManager = NEVPNManager.shared()
        
        vpnManager?.loadFromPreferences(completionHandler: {[weak self] error  in
            guard let the_self = self else { return }
            
            if let the_error = error{
                the_self.delegate?.vpnLoaded(result: VPNStructs.VPNResults.failure , message: "Load error: \(the_error)")
                print("Load error: \(the_error)")
            } else {
                the_self.delegate?.vpnLoaded(result: VPNStructs.VPNResults.success , message: "Loaded")
                // No errors! The rest of your codes goes here...
            }
        })

    }
    
    // setup vpn and try to save it to device
    func setup()
    {
        
        if !self.checkInput() { return }
        
        //KCStore.clear()
        KCStore.setStringToKeychain(object: password!, forKey: .password_key)
        
        let p = NEVPNProtocolIPSec.init()
        p.username = username!;   //"Rufat A"
        p.passwordReference = KCStore.getRefFromKeychain(forKey: .password_key)
        p.serverAddress = serverAddress!
        
        if authenticationMethod! == .sharedSecret {
            p.authenticationMethod =  .sharedSecret
            p.sharedSecretReference = KCStore.getRefFromKeychain(forKey: .shared_secret_key)
        }else{
            p.authenticationMethod =  .certificate
            
            //load data from provided key if given
            if let the_key = keyInData{
                p.identityData = the_key
            }else{
                //load from URL link in text field
                if let url = URL(string: certUrl!) {
                    do {
                        let contents = try Data(contentsOf: url)
                        p.identityData = contents
                    } catch {
                        // contents could not be loaded
                        self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "*.p12 contents could not be loaded")
                        return
                    }
                } else {
                    // the URL was bad!
                    self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "the URL was bad")
                    return
                }
            }
        }
        
        
        p.localIdentifier = UIDevice.current.name
        p.remoteIdentifier = remoteIdentifier!;  //"Rufats-MBP"
        p.useExtendedAuthentication = true
        p.disconnectOnSleep = false
        
        vpnManager?.protocolConfiguration = p
        vpnManager?.isOnDemandEnabled = false
        vpnManager?.localizedDescription = localizedDescription!;    //"Connection to my own MBP"
        vpnManager?.saveToPreferences(completionHandler: {[weak self] error in
            guard let the_self = self else { return }
            
            if let the_error = error {
                the_self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Save error: \(the_error)")
                print("Save error: \(the_error)")
            } else {
                the_self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.success , message: "Saved")
                // No errors! The rest of your codes goes here...
            }
        })
        
    }
    
    // connect to vpn server
    func connect()
    {
        do {
            try vpnManager?.connection.startVPNTunnel()
                        
            if vpnManager?.connection.status == NEVPNStatus.connected {
                self.delegate?.vpnConnected(result: VPNStructs.VPNResults.success , message: "Connection established!")
                print("Connection established!")
            }
            
            
        } catch let error as NSError {
            self.delegate?.vpnConnected(result: VPNStructs.VPNResults.failure , message: "Failed to convect: \(error.localizedDescription)")
            print("Failed to convect: \(error.localizedDescription)")
        }
    }
    
    // disconnect from vpn server
    func disconnect()
    {
        vpnManager?.connection.stopVPNTunnel()
    }
    
    // add ovserver to monotor changes in vpn connection
    func addVPNObserver()->NSObjectProtocol?
    {
        
        // Register to receive notification
        return NotificationCenter.default.addObserver(forName: NSNotification.Name.NEVPNStatusDidChange,
                                                      object: nil,
                                                      queue: OperationQueue.main,
                                                      using: { [weak self] notification in
                                                        guard let the_self = self else { return }
                                                        the_self.receivedVPNhNotification(notification: notification)
        })
        
    }
    
    // stop observing vpn changes
    func removeVPNObserver()
    {
        NotificationCenter.default.removeObserver(self, name:  NSNotification.Name.NEVPNStatusDidChange, object: nil)
    }
    
    // after receiving vpn status change update delegate on that
    func receivedVPNhNotification(notification: Notification?)
    {
        if let the_object = notification?.object as? NEVPNConnection{
            self.delegate?.vpnStatusChanged(status: the_object.status)
        }
    }
    
    // simple validation of input data for vpn connection
    func checkInput()->Bool
    {
        
        if let the_userame = username,
            !the_userame.isEmpty{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Username undefined");
            return false
        }
        
        if let the_password = password,
            !the_password.isEmpty{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Password undefined");
            return false
        }

        if let the_serverAddress = serverAddress,
            !the_serverAddress.isEmpty{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Server Address undefined");
            return false
        }

        
        if let the_remoteIdentifier = remoteIdentifier,
            !the_remoteIdentifier.isEmpty{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Remote Identifier undefined");
            return false
        }

        
        if let the_localizedDescription = localizedDescription,
            !the_localizedDescription.isEmpty{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Descrption undefined");
            return false
        }

        
        if let _ = authenticationMethod{
            //ok
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Authentication undefined");
            return false
        }

        
        if authenticationMethod! == .sharedSecret {
            if let the_sharedSecret = sharedSecret,
                !the_sharedSecret.isEmpty{
                //ok
                KCStore.setStringToKeychain(object: the_sharedSecret, forKey: .shared_secret_key)
            }else{
                self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Shared Secret undefined");
                return false
            }
        }else if authenticationMethod! == .certificate {
            
            if let _ = keyInData {
                return true
            }
            
            if let the_sharedSecret = sharedSecret,
                !the_sharedSecret.isEmpty{
                //ok
                self.certUrl = the_sharedSecret
                
                if !(the_sharedSecret.hasPrefix("https")) {
                    self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Certificate URL must start from HTTPS ");
                    return false
                
                }
                
            }else{
                self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Certificate undefined");
                return false
            }
        }else{
            self.delegate?.vpnConfigured(result: VPNStructs.VPNResults.failure , message: "Authentication not selected undefined");
            return false
        }
        
        return true
    }
    
}
