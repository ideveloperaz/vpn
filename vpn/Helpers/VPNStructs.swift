//
//  VPNStructs.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit
import NetworkExtension


class VPNStructs: NSObject
{

    enum KeychainKeys: String
    {
        case password_key = "password_key"
        case shared_secret_key = "shared_secret_key"
    }

    enum VPNResults: String
    {
        case success = "SUCCESS"
        case failure = "FAILURE"
    }
    
    enum Segues: String
    {
        case showSavedKeys = "show-saved-keys"
    }

}


protocol VPNDelegateProtocol: class
{
    func vpnLoaded(result: VPNStructs.VPNResults, message: String)
    func vpnConfigured(result: VPNStructs.VPNResults, message: String)
    func vpnConnected(result: VPNStructs.VPNResults, message: String)
    func vpnStatusChanged(status: NEVPNStatus)
}

protocol VPNKeyDelegate: class
{
    func useSavedKey(data: Data, fname: String)
}

