//
//  VPNTable.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit

class VPNTable: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnClose: UIButton!

    var rowsNum = 1
    var savedKeys: [String]?
    weak var parentVC: VPNKeyDelegate?
    
    var newKeyUrl, newKeyName: String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.load()
        
        // dismiss this controller by tapping around table
        self.dismissControllerWhenTappedAround()

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return rowsNum
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell: UITableViewCell
            
        // Configure the cell...
        
        if indexPath.row == (rowsNum-1) {
            //add new key cell
            cell = tableView.dequeueReusableCell(withIdentifier: "newReuseIdentifier", for: indexPath)
            let lbl = cell.contentView.viewWithTag(1) as? UILabel

            lbl?.text = "SAVE AS A NEW KEY"
        }else{
            //saved key cell
            let vpnCell = tableView.dequeueReusableCell(withIdentifier: VPNTableCell.reuseIdentifier(), for: indexPath) as! VPNTableCell

            // reference to this table in order to call methods from cell (keep it simple :-) )
            vpnCell.vpnTable = self
            
            if let the_keys = savedKeys{
                vpnCell.lbl?.text = the_keys[indexPath.row]
                vpnCell.btnDeleteKey.tag = indexPath.row
            }
            
            cell = vpnCell
        }

        return cell
    }
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == (rowsNum-1) {
            //add new address cell
            
            tableView.deselectRow(at: indexPath, animated: true)

            self.saveNewKey()
            
            self.dismiss(animated: true, completion: {})
            
        }else{
            //saved key cells

            if let could_load_key = self.parentVC,
                let the_file = savedKeys?[indexPath.row],
                let the_data = VPNFileManager.readFile(fname: the_file){
                could_load_key.useSavedKey(data: the_data, fname: the_file)
            }
        
            self.dismiss(animated: true, completion: {})
        }
        
    }

    // MARK: - VPNKeyDelegate protocol
    
    func passSelectedAddress(address:[String:String]?, givenGeocodeType: String)
    {
        self.load()
    }

    
    // MARK: - User Defined

    // load keys from document folder at local device
    func load()
    {
        self.savedKeys = VPNFileManager.getKeys()
        rowsNum = 1 + (self.savedKeys?.count ?? 0);    // +1 because of Add New Key cell
        self.tableView.reloadData()
    }

    
    // save key by loading it from secure web server and save it with protection enabled, key must be in p12 format
    func saveNewKey()
    {
        
        guard let the_newKeyName = self.newKeyName else { return }
        
        if let url = URL(string: self.newKeyUrl!) {
            do {
                let contents = try Data(contentsOf: url)
                //save contents here
                VPNFileManager.writeToFile(dataToWrite: contents, fname: the_newKeyName)
            } catch {
                // contents could not be loaded
                let alert = UIAlertController(title: "Error", message: "*.p12 contents could not be loaded", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

                return
            }
        } else {
            // the URL was bad!
            let alert = UIAlertController(title: "Error", message: "the URL was bad", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

            return
        }
    }
    
    // delete key from local storage
    func deleteKey(atIndex: Int)
    {
        if let the_file = savedKeys?[atIndex] {
            VPNFileManager.deleteFile(fname: the_file)
            
            self.load()
        }
    }

}
