//
//  ViewController.swift
//  vpn
//
//  Created by Rufat A on 12/9/16.
//  Copyright © 2016 Rufat A. All rights reserved.
//

import UIKit
import NetworkExtension

class ViewController: UIViewController, VPNDelegateProtocol, UITextFieldDelegate, VPNKeyDelegate {

    var vpn: VPNManager?                        // vpn class that responds for vpn stuff
    private var vpnObserver: NSObjectProtocol?  // vpn status change observer

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtServerAddress: UITextField!
    @IBOutlet weak var sgAuthType: UISegmentedControl!
    @IBOutlet weak var txtSharedSecret: UITextField!    // could be sharedSectet, Certificate url or name if taken from local storage
    @IBOutlet weak var txtRemoteId: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var lblAuthentication: UILabel!
    @IBOutlet weak var btnSaveKey: UIButton!

    var isUseFromStorage = false;   // use loaded key from storage or not
    var keyData: Data?              // key loaded from local device
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        hideKeyboardWhenTappedAround()        

        // init vpn variable here
        vpn = VPNManager.init()
        vpn?.delegate = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func load(){
        vpn?.username = txtUsername.text
        vpn?.serverAddress = txtServerAddress.text
        vpn?.remoteIdentifier = txtRemoteId.text
        vpn?.localizedDescription = txtDescription.text
        vpn?.password = txtPassword.text
        vpn?.sharedSecret = txtSharedSecret.text
        if sgAuthType.selectedSegmentIndex == 0 {
            vpn?.authenticationMethod = NEVPNIKEAuthenticationMethod.sharedSecret
        }else{
            vpn?.authenticationMethod = NEVPNIKEAuthenticationMethod.certificate
        }
        
        // configure vpn with given key from local file or by control data
        if let the_key = keyData{
            vpn?.load(withKey: the_key)
        }else{
            vpn?.load(withKey: nil)
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == VPNStructs.Segues.showSavedKeys.rawValue{
            if let tableVC = segue.destination as? VPNTable{
                //addressVC.addressFromParentVC = self.reverseGeoAddress
                tableVC.parentVC = self
                tableVC.newKeyUrl = txtSharedSecret.text    // pass certificate url and name fopr saving at local storage
                tableVC.newKeyName = txtDescription.text
            }
        }
        
    }
    
    // MARK: VPNDelegate
    
    // after initial vpn stuff load try to setup it
    func vpnLoaded(result: VPNStructs.VPNResults, message: String) {
        if result == .success{
            vpn?.setup()
        }else{
            //on failure notify user
            let alert = UIAlertController(title: result.rawValue, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // after setup of vpn try to connect to server
    func vpnConfigured(result: VPNStructs.VPNResults, message: String) {
        if result == .success{
            vpn?.connect()
        }else{
            //on failure notify user
            setControle(status: true)

            let alert = UIAlertController(title: result.rawValue, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // pn successful connection to vpn do whatever needed
    func vpnConnected(result: VPNStructs.VPNResults, message: String) {

        let alert = UIAlertController(title: result.rawValue, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // monitor status change, callback from vpn class
    func vpnStatusChanged(status: NEVPNStatus) {
        //do something here
        setControle(status: true)
        
        var msg = ""
        
        switch status {
        case .connected:
            msg = "VPN Connected"
        case .connecting:
            setControle(status: false)
            msg = "VPN connecting"
        case .disconnected:
            msg = "VPN disconnected"
        case .disconnecting:
            msg = "VPN disconnecting"
        case .reasserting:
            msg = "VPN reasserting"
        case .invalid:
            msg = "VPN invalid"
        }
        
        print(" --------- \(msg) ---------- ")
        
    }
    
    // MARK: VPNKeyDelegate
    
    // called when stored key loaded for reuse
    
    func useSavedKey(data: Data, fname: String) {
        self.isUseFromStorage = true
        self.keyData = data
        
        lblAuthentication.text = "Certificate"
        sgAuthType.selectedSegmentIndex = 1;    //select Certificate
        txtSharedSecret.text = fname
    }
    
    // MARK: UserDefined

    // tru to connect to vpn using entered data
    @IBAction func btnConnectClicked(_ sender: UIButton) {
        
        setControle(status: false)
        self.load()
        
    }
    
    // disconnect from vpn server
    @IBAction func btnDisconnectClicked(_ sender: UIButton) {
        setControle(status: true)
        vpn?.disconnect()
    }
    
    // just to play with controlls on status change 
    func setControle(status: Bool){
        self.indicator.isHidden = status
        self.btnConnect.isEnabled = status
    }

    // segment control changes value
    @IBAction func sgChanged(_ sender: UISegmentedControl) {
        
        self.isUseFromStorage = false
        self.keyData = nil

        if sgAuthType.selectedSegmentIndex == 0 {
            lblAuthentication.text = "Shared Secret"
            txtSharedSecret.placeholder = "Enter Shared Secret"
            txtSharedSecret.text = nil
            txtSharedSecret.isSecureTextEntry = true
            btnSaveKey.isHidden = true
        }else{
            lblAuthentication.text = "Certificate"
            txtSharedSecret.placeholder = "URL to *.p12 at HTTPS"
            txtSharedSecret.text = nil
            txtSharedSecret.isSecureTextEntry = false
            btnSaveKey.isHidden = false
        }
    }
}

